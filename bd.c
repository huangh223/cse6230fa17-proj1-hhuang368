#include <stdlib.h>
#include <stdio.h>
#include <unistd.h> // access
#include <math.h>
#include <assert.h>
#include "bd.h"

#include <string.h>
#include <omp.h>

cse6230nrand_t *nrand_handles = NULL;

int bd(int npos, double *pos, double L, const int *types, int *maxnumpairs_p, double **dist2_p, int **pairs_p)
{
	double f = sqrt(2.*DELTAT);
	/* 2 is twice the radius, $2 r_c$ in Prof. Chow's Lecture 5 */
	int		 boxdim = L / 2;
	/* Must be at least the square of twice the radius */
	double cutoff2 = 4.;
	int	   maxnumpairs = *maxnumpairs_p;
	double *dist2 = *dist2_p;
	//int	   *pairs = *pairs_p;
	
	int nthreads = omp_get_max_threads();
	
	// allocate buffers for each thread
	double *dist  = (double*) malloc(sizeof(double) * maxnumpairs * 1 * nthreads);
	int    *pairs = (int*)    malloc(sizeof(int)    * maxnumpairs * 2 * nthreads);
	double *dxyz  = (double*) malloc(sizeof(double) * maxnumpairs * 3 * nthreads);
	int    *t_np  = (int*)    malloc(sizeof(int)    * nthreads);
	assert(dxyz != NULL && dist != NULL && pairs != NULL && t_np != NULL);
	if (nrand_handles == NULL)
	{
		nrand_handles = (cse6230nrand_t *) malloc(sizeof(cse6230nrand_t) * 64);
		assert(nrand_handles != NULL);
		for (int i = 0; i < 64; i++) cse6230nrand_seed(666 + i, nrand_handles + i);
	}
	
	// each thread init its own buff, deal with first touch
	#pragma omp parallel
	{
		int tid = omp_get_thread_num();
		memset(dist  + tid * maxnumpairs,     0, sizeof(double) * maxnumpairs * 1);
		memset(pairs + tid * maxnumpairs * 2, 0, sizeof(int)    * maxnumpairs * 2);
		memset(dxyz  + tid * maxnumpairs * 3, 0, sizeof(double) * maxnumpairs * 3);
	}

	double ia_t = 0.0, af_t = 0.0, rw_t = 0.0, st, et;
	set_ia_timer();

	for (int step=0; step<INTERVAL_LEN; step++)
	{
		int retval;
		int numpairs = 0;

		while (1) 
		{
			st = omp_get_wtime();
			retval = interactions(npos, pos, L, boxdim, cutoff2, pairs, maxnumpairs, &numpairs, dxyz, dist, t_np);
			et = omp_get_wtime();
			ia_t += et - st;
			if (!retval) break;
			if (retval == -1) 
			{
				free(pairs);
				free(dist);
				free(dxyz);
				maxnumpairs *= 2;
				dist  = (double*) malloc(sizeof(double) * maxnumpairs * 1 * nthreads);
				pairs = (int*)    malloc(sizeof(int)    * maxnumpairs * 2 * nthreads);
				dxyz  = (double*) malloc(sizeof(double) * maxnumpairs * 3 * nthreads);
				assert(dxyz != NULL && dist != NULL && pairs != NULL && t_np != NULL);
				#ifdef DBGOUT
				printf("[DEBUG] Multithread buffer allocated: buffer size = %d\n", maxnumpairs * nthreads);
				#endif
			} else {
				return retval;
			}
		}
		
		// in interactions(), each thread use its own buff to save results
		// to save time, I do not merge those results into one array, just 
		// traversal each thread's buff and pick up those results
		st = omp_get_wtime();
		for (int tid = 0; tid < nthreads; tid++)
		{
			double *t_dist = dist  + tid * maxnumpairs;
			double *t_dxyz = dxyz  + tid * maxnumpairs * 3;
			int  *t_pairs  = pairs + tid * maxnumpairs * 2;
			for (int p = 0; p < t_np[tid]; p++)
			{
				// dx, dy, dz is calculated while searching interaction pairs,
				// so just accumulate the results
				double dx     = t_dxyz[3 * p];
				double dy     = t_dxyz[3 * p + 1];
				double dz     = t_dxyz[3 * p + 2];
				int i = t_pairs[2 * p], j = t_pairs[2 * p + 1];
				
				pos[3 * i]     += dx; 
				pos[3 * i + 1] += dy;
				pos[3 * i + 2] += dz; 
				
				pos[3 * j]     -= dx;
				pos[3 * j + 1] -= dy; 
				pos[3 * j + 2] -= dz;
			}
		}
		et = omp_get_wtime();
		af_t += et - st;

		// update positions with Brownian displacements
		st = omp_get_wtime();
		#pragma omp parallel
		{
			int tid = omp_get_thread_num();
			
			#pragma omp for
			for (int i = 0; i < 3 * npos; i++)
				pos[i] += f * cse6230nrand(nrand_handles + tid);
		}
		et = omp_get_wtime();
		rw_t += et - st;
	}

	#ifdef DBGOUT
	printf("[DEBUG] IA / AF / RW time = %lf, %lf, %lf\n", ia_t, af_t, rw_t);
	end_ia_timer();
	#endif

	*maxnumpairs_p = maxnumpairs;
	*dist2_p = dist2;
	//*pairs_p = pairs;
	
	free(dxyz);
	free(dist);
	free(pairs);
	free(t_np);

	return 0;
}
