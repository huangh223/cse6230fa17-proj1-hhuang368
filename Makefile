
CC = gcc
CSE6230UTILSDIR = ./utils
#CFLAGS = -g -Wall -O3 -std=gnu99
CFLAGS = -O3 -std=gnu99
CPPFLAGS = -I$(CSE6230UTILSDIR) -I$(CSE6230UTILSDIR)/tictoc
OMPFLAGS = -fopenmp
LIBS = -lm
RM = rm -f
INFILE = 10000.xyz
#INFILE = lac1_novl2.xyz
OUTFILE = out.xyz
NINTERVALS = 20
HARNESS_ENV = GOMP_CPU_AFFINITY=0-31 OMP_NUM_THREADS=32

all: runharness

%.o: %.c
	$(CC) $(CPPFLAGS) $(CFLAGS) $(OMPFLAGS) -c -o $@ $<

harness: harness.o bd.o interactions.o
	$(CC) $(OMPFLAGS) -o $@ $^ $(LIBS)

runharness: harness
	$(RM) $(OUTFILE) && $(HARNESS_ENV) ./harness $(INFILE) $(OUTFILE) $(NINTERVALS)

clean:
	$(RM) *.o harness

.PHONY: clean runharness

