function proj1_plot(runtime)
	clf;
	
	semilogy(1 : 6, runtime, 'r*-'), grid on, xlabel('Threads'), ylabel('Time (second)'), hold on
	set(gca, 'xtick', [1 2 3 4 5 6]), hold on
	set(gca, 'xticklabel', {'1', '2', '4', '8', '16', '32'}), hold on
	text(1, runtime(1), ['(1, ' num2str(runtime(1)) ')'])
	text(2, runtime(2), ['(2, ' num2str(runtime(2)) ')'])
	text(3, runtime(3), ['(4, ' num2str(runtime(3)) ')'])
	text(4, runtime(4), ['(8, ' num2str(runtime(4)) ')'])
	text(5, runtime(5), ['(16, ' num2str(runtime(5)) ')'])
	text(6, runtime(6), ['(32, ' num2str(runtime(6)) ')'])
	axis([1, 6, 10, 150]), hold on
	title('Relationship between number of threads and running time, NINTERVALS=20')
end