#!/bin/sh
#SBATCH --job-name=my_harness            # Job name
#SBATCH --time=00:30:00                  # Time limit hrs:min:sec
#SBATCH --nodes=1                        # Just one node, but
#SBATCH --exclusive                      # My node alone
#SBATCH --output=harness_%j.out          # Standard output and error log

pwd; hostname; date

#make

for ((i = 1; i <= 32; i = i * 2))
do
	rm -f out.xyz 
	echo Using $i threads
	GOMP_CPU_AFFINITY=0-31 OMP_NUM_THREADS=$i ./harness 10000.xyz out.xyz 20
done

date
