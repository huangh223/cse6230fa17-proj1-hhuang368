#include <math.h>
#include <stdlib.h>
#include <stdio.h>

const double inv_RAND_MAX = 1.0 / (double) RAND_MAX;

inline double rand_double(const double min, const double max)
{
	double range = max - min;
	double scale = (double)rand() * inv_RAND_MAX;
	return (min + scale * range);
}

int main()
{
	int N;
	double frac_raito;
	char fname[255];
	
	printf("Paricle numbers: ");
	scanf("%d", &N);
	
	printf("Fraction ratio: ");
	scanf("%lf", &frac_raito);
	
	printf("Output file name: ");
	scanf("%s", fname);
	
	double pr_vol = (4.0 / 3.0) * M_PI;
	double domain_vol = pr_vol * N / frac_raito;
	double box_width = pow(domain_vol, 1.0 / 3.0);
	
	printf("Box width = %lf\n", box_width);
	
	FILE *ouf = fopen(fname, "w");
	fprintf(ouf, "%d\n", N);
	fprintf(ouf, "%lf %lf\n", 0.0, box_width);
	for (int i = 0; i < N; i++)
	{
		double x = rand_double(0.0, box_width);
		double y = rand_double(0.0, box_width);
		double z = rand_double(0.0, box_width);
		fprintf(ouf, "0 %lf %lf %lf\n", x, y, z);
	}
	fclose(ouf);
	
	printf("Generating %d random position in box done\n", N);
	
	return 0;
}