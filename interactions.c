#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "bd.h"

#include <string.h>
#include <omp.h>

struct box
{
	int head;
};

// it is possible to use smaller boxes and more complex neighbor patterns
#define NUM_BOX_NEIGHBORS 13
int box_neighbors[NUM_BOX_NEIGHBORS][3] =
{
	{-1,-1,-1},
	{-1,-1, 0},
	{-1,-1,+1},
	{-1, 0,-1},
	{-1, 0, 0},
	{-1, 0,+1},
	{-1,+1,-1},
	{-1,+1, 0},
	{-1,+1,+1},
	{ 0,-1,-1},
	{ 0,-1, 0},
	{ 0,-1,+1},
	{ 0, 0,-1}
};

static int interactions_check(int npos, const double *pos, double L, double cutoff2, int numpairs, const int *pairs)
{
	int intcount = 0;

	for (int i = 0; i < npos; i++) 
	{
		for (int j = i + 1; j < npos; j++) 
		{
			double dx = remainder(pos[3*i+0] - pos[3*j+0],L);
			double dy = remainder(pos[3*i+1] - pos[3*j+1],L);
			double dz = remainder(pos[3*i+2] - pos[3*j+2],L);
			if (dx*dx + dy*dy + dz*dz < cutoff2) 
			{
				intcount++;
				int k;
				for (k = 0; k < numpairs; k++) 
				{
					if ((pairs[2*k + 0] == i && pairs[2*k + 1] == j) ||
						(pairs[2*k + 0] == j && pairs[2*k + 1] == i)) 
					{
						break;
					}
				}
				assert(k<numpairs);
			}
		}
	}
	return intcount;
}

// interactions function
//
// Construct a list of particle pairs within a cutoff distance
// using Verlet cell lists.  The L*L*L domain is divided into
// boxdim*boxdim*boxdim cells.  We require cutoff < L/boxdim
// and boxdim >= 4.  Periodic boundaries are used.
// Square of distance is also returned for each pair.
// Note that only one of (i,j) and (j,i) are returned (not both).
// The output is not sorted by index in any way.
//
// npos = number of particles
// pos  = positions stored as [pos1x pos1y pos1z pos2x ...]
// L	= length of one side of box
// boxdim = number of cells on one side of box
// cutoff2 = square of cutoff
// distances2[maxnumpairs] = OUTPUT square of distances for particles
//						   within cutoff
// pairs[maxnumpairs*2] = OUTPUT particle pairs stored as
//						[pair1i pair1j pair2i pair2j ...]
// maxnumpairs = max number of pairs that can be stored in user-provided arrays
// numpairs_p = pointer to actual number of pairs (OUTPUT)
//
// function returns 0 if successful, or nonzero if error occured

inline void assign_particles_to_boxes(
	int npos, int boxdim, double L, 
	const double *pos, int *next, int *box_head, 
	int *pos_in_box, int tid
)
{
	// traverse all particles and assign to boxes
	#pragma omp for
	for (int i = 0; i < npos; i++)
	{
		// initialize entry of implied linked list
		double pos_p[3];
		next[i] = -1;
		for (int j = 0; j < 3; j++) 
			pos_p[j] = remainder(pos[3*i+j]-L/2.,L) + L/2.;
	  
		// calc which box does the particle belong to
		int idx = (int)(pos_p[0]/L*boxdim);
		int idy = (int)(pos_p[1]/L*boxdim);
		int idz = (int)(pos_p[2]/L*boxdim);
		
		pos_in_box[i] = idx*boxdim*boxdim + idy*boxdim + idz;
	}
	
	// construct linked list for each box
	#pragma omp single
	for (int i = 0; i < npos; i++)
	{
		int box_head_pos = pos_in_box[i];
		next[i] = box_head[box_head_pos];
		box_head[box_head_pos] = i;
	}
}

void check_current_box_interactions(
	int curr_idx, double L, double cutoff2, 
	int *box_head, int *next, const double *pos,
	int *pairs, double *dist, double *dxyz, 
	int maxnumpairs, int *numpairs, int *queue_not_full
)
{
	int p1 = box_head[curr_idx];
	while (p1 != -1)
	{
		int p2 = next[p1];
		while (p2 != -1)
		{
			// do not need minimum image since we are in same box
			double dx = remainder(pos[3*p1+0] - pos[3*p2+0],L);
			double dy = remainder(pos[3*p1+1] - pos[3*p2+1],L);
			double dz = remainder(pos[3*p1+2] - pos[3*p2+2],L);
			double d2 = dx*dx+dy*dy+dz*dz;

			if (d2 < cutoff2)
			{
				// calculate the replusive force here 
				double d = sqrt(d2);
				double inv_s  = 1.0 / d;
				double F      = 100.0 * (2.0 - d);
				double F_dt_inv_s = F * inv_s * DELTAT;
				dx *= F_dt_inv_s;
				dy *= F_dt_inv_s;
				dz *= F_dt_inv_s;
				
				// save interaction pair info and move distance 
				// caused by replusive force 
				pairs[2 * (*numpairs)]     = p1;
				pairs[2 * (*numpairs) + 1] = p2;
				dxyz[3 * (*numpairs)]      = dx;
				dxyz[3 * (*numpairs) + 1]  = dy;
				dxyz[3 * (*numpairs) + 2]  = dz;
				
				// check whether the result buff is full
				(*numpairs)++;
				if ((*numpairs) >= maxnumpairs)
				{	
					(*queue_not_full) = 0;
					return;
				}
			}

			p2 = next[p2];
		}
		p1 = next[p1];
	}
}

int *box_neighbors_idx = NULL;

void check_neighbor_box_interactions(
	int curr_idx, double L, double cutoff2, 
	int *box_head, int *next, const double *pos,
	int *pairs, double *dist, double *dxyz, 
	int maxnumpairs, int *numpairs, int *queue_not_full
)
{
	for (int j = 0; j < NUM_BOX_NEIGHBORS; j++)
	{
		int p1 = box_head[box_neighbors_idx[curr_idx * NUM_BOX_NEIGHBORS + j]];
		while (p1 != -1)
		{
			int p2 = box_head[curr_idx];
			while (p2 != -1)
			{
				// compute distance vector
				double dx = remainder(pos[3*p1+0] - pos[3*p2+0],L);
				double dy = remainder(pos[3*p1+1] - pos[3*p2+1],L);
				double dz = remainder(pos[3*p1+2] - pos[3*p2+2],L);
				double d2 = dx*dx+dy*dy+dz*dz;

				if (d2 < cutoff2)
				{
					// calculate the replusive force here 
					double d = sqrt(d2);
					double inv_s  = 1.0 / d;
					double F      = 100.0 * (2.0 - d);
					double F_dt_inv_s = F * inv_s * DELTAT;
					dx *= F_dt_inv_s;
					dy *= F_dt_inv_s;
					dz *= F_dt_inv_s;
					
					// save interaction pair info and move distance 
					// caused by replusive force 
					pairs[2 * (*numpairs)]     = p1;
					pairs[2 * (*numpairs) + 1] = p2;
					dxyz[3 * (*numpairs)]      = dx;
					dxyz[3 * (*numpairs) + 1]  = dy;
					dxyz[3 * (*numpairs) + 2]  = dz;
					
					// check whether the result buff is full
					(*numpairs)++;
					if ((*numpairs) >= maxnumpairs) 
					{
						(*queue_not_full) = 0;
						return;
					}
				}

				p2 = next[p2];
			}
			p1 = next[p1];
		}
	}
}

//====== auxilliary spaces for box =====//
int _boxdim = 0, _npos = 0;
int *box_next = NULL, *box_head = NULL;
int *pos_in_box;
//======================================//

void calc_box_neighbours()
{
	for (int idx = 0; idx < _boxdim; idx++)
		for (int idy = 0; idy < _boxdim; idy++)
			for (int idz = 0; idz < _boxdim; idz++)
			{
				int curr_index = idx*_boxdim*_boxdim + idy*_boxdim + idz;
				
				for (int j = 0; j < NUM_BOX_NEIGHBORS; j++)
				{
					int neigh_idx = (idx + box_neighbors[j][0] + _boxdim) % _boxdim;
					int neigh_idy = (idy + box_neighbors[j][1] + _boxdim) % _boxdim;
					int neigh_idz = (idz + box_neighbors[j][2] + _boxdim) % _boxdim;
					
					int neigh_index = neigh_idx*_boxdim*_boxdim + neigh_idy*_boxdim + neigh_idz;
					
					box_neighbors_idx[curr_index * NUM_BOX_NEIGHBORS + j] = neigh_index;
				}
			}
}

void check_and_init_box_arrays(int boxdim, double cutoff2, double L, int npos)
{
	assert(! (boxdim < 4 || cutoff2 > (L/boxdim)*(L/boxdim)));

	if (npos > _npos)
	{
		if (box_next != NULL) free(box_next);
		box_next   = (int *) malloc(npos*sizeof(int));
		pos_in_box = (int *) malloc(npos*sizeof(int));
		assert(box_next != NULL && pos_in_box != NULL);
		_npos = npos; 
	}
	
	if (boxdim > _boxdim)
	{
		if (box_head != NULL) free(box_head);
		if (box_neighbors_idx != NULL) free(box_neighbors_idx);
		box_head = (int *) malloc(sizeof(int) * boxdim*boxdim*boxdim);
		box_neighbors_idx = (int *) malloc(sizeof(int) * NUM_BOX_NEIGHBORS * boxdim*boxdim*boxdim);
		assert(box_head != NULL && box_neighbors_idx != NULL);
		_boxdim = boxdim;
		calc_box_neighbours();
	}
	
	for (int i = 0; i < boxdim*boxdim*boxdim; i++) box_head[i] = -1;
}

double single_t, para_t;

void set_ia_timer()
{
	single_t = para_t = 0.0;
}

void end_ia_timer()
{
	#ifdef DBGOUT
	printf("[DEBUG] interactions(): serial / para region time = %lf, %lf\n", single_t, para_t);
	#endif
}

int interactions(int npos, const double *pos, double L, int boxdim,
				 double cutoff2, int *pairs, int maxnumpairs, int *numpairs_p,
				 double *dxyz, double *dist, int *t_np)
{
	double st, et;
	st = omp_get_wtime();
	// check input arguments and alloc local buff 
	check_and_init_box_arrays(boxdim, cutoff2, L, npos);
	et = omp_get_wtime();
	single_t += et - st;
	
	int numpairs = 0;
	int queue_not_full = 1;
	int boxdim2 = boxdim*boxdim;
	
	st = omp_get_wtime();
	#pragma omp parallel
	{
		// each thread use its own buff to save results
		int tid = omp_get_thread_num();
		double *t_dist = dist  + tid * maxnumpairs;
		double *t_dxyz = dxyz  + tid * maxnumpairs * 3;
		int *t_pairs   = pairs + tid * maxnumpairs * 2;
		t_np[tid] = 0;
		
		assign_particles_to_boxes(
			npos, boxdim, L, pos, box_next, box_head, 
			pos_in_box, tid
		);
		
		#pragma omp for 
		for (int box_id = 0; box_id < boxdim*boxdim*boxdim; box_id++)
		{
			if (box_head[box_id] == -1) continue;
			if (queue_not_full == 0) continue;

			// within box interactions
			check_current_box_interactions(
				box_id, L, cutoff2,
				box_head, box_next, pos,
				t_pairs, t_dist, t_dxyz, 
				maxnumpairs, &t_np[tid], &queue_not_full
			);

			// interactions with other boxes
			check_neighbor_box_interactions(
				box_id, L, cutoff2,
				box_head, box_next, pos,
				t_pairs, t_dist, t_dxyz, 
				maxnumpairs, &t_np[tid], &queue_not_full
			);
		}
	}
	et = omp_get_wtime();
	para_t += et - st;

	*numpairs_p = numpairs;
	if (queue_not_full == 0) return -1;

	#if 0  // the following sanity check cannot be used for new data structure
	{
		int numpairscheck = interactions_check(npos,pos,L,cutoff2,numpairs,pairs);
		assert(numpairs == numpairscheck);
	}
	#endif

	return 0;
}


#if defined(INTERACTIONS_MEX)
#include "mex.h"

// matlab: function pairs = interactions(pos, L, boxdim, cutoff)
//  assumes pos is in [0,L]^3
//  assumes boxdim >= 4
//  pairs is an int array, may need to convert to double for matlab use
void
mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	int npos;
	const double *pos;
	double L;
	int boxdim;
	double cutoff;

	npos   = mxGetN(prhs[0]);
	pos	= mxGetPr(prhs[0]);
	L	  = mxGetScalar(prhs[1]);
	boxdim = (int) mxGetScalar(prhs[2]);
	cutoff = mxGetScalar(prhs[3]);

	// estimate max number of pairs, assuming somewhat uniform distribution
	double ave_per_box = npos / (double)(boxdim*boxdim*boxdim) + 1.;
	int maxnumpairs = (int) (0.7*npos*ave_per_box*(NUM_BOX_NEIGHBORS+1) + 1.);
	printf("interactions: maxnumpairs: %d\n", maxnumpairs);

	// allocate storage for output
	int *pairs = (int *) malloc(2*maxnumpairs*sizeof(int));
	double *distances2 = (double *) malloc(maxnumpairs*sizeof(double));
	if (pairs == NULL || distances2 == NULL)
	{
		printf("interactions: could not allocate storage\n");
		return;
	}

	int numpairs; // actual number of pairs
	int ret;
	ret = interactions(npos, pos, L, boxdim, cutoff*cutoff, distances2,
					   pairs, maxnumpairs, &numpairs);
	if (ret != 0)
	{
		printf("interactions: error occured\n");
		if (ret == -1)
			printf("interactions: estimate of required storage was too low\n");
		return;
	}
	printf("interactions: numpairs: %d\n", numpairs);

	// allocate matlab output matrix
	plhs[0] = mxCreateDoubleMatrix(numpairs, 3, mxREAL);
	double *data = (double *) mxGetPr(plhs[0]);

	// first col of data array is row indices
	// second col of data array is col indices
	// third col of data array is distance2
	int i;
	for (i=0; i<numpairs; i++)
	{
		data[i]			= pairs[2*i];
		data[i+numpairs]   = pairs[2*i+1];
		data[i+numpairs*2] = distances2[i];
	}

	free(pairs);
	free(distances2);
}
#endif

#if defined(INTERACTIONS_MAIN)
int main()
{
	int npos = 3;
	double pos[] = {0., 0., 0.,  0., 0., 3.5,  0., 3.5, 0.};
	double L = 8;
	int pairs[1000*2];
	int numpairs;

	interactions(npos, pos, L, 8, 99., pairs, 1000, &numpairs);

	printf("numpairs %d\n", numpairs);

	return 0; }
#endif
